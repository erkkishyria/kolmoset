// Idea oli, 
// jos luku on jaollinen kolmella tulosta Fizz
// jos luku on jaollinen viidellä tulosta Buzz
// jos luku on jaollinen sekä viidellä, että kolmella tulosta FizzBuzz

for (let index = 1; index <= 100; index++) {
  if(index % 3 === 0 && index % 5 === 0) {
    console.log("FizzBuzz");
  } else if(index % 3 === 0) {
    console.log("Fizz");
  } else if(index % 5 === 0) {
    console.log("Buzz");
  } else {
    console.log(index);
  }
}