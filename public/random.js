
/**
 * Funktio eri toimintojen kokeilemista varten. 
 */
function toiminne() {
    let color = window.prompt("What is your favorite color?", "blue");
    if(color == null) {
        color = "..cancelled (?)";
    }
    alert(`Your favorite color is ${color}`);
    let capital = window.prompt("What is the capital of Assyria?", "I don't know");
    let element = document.createElement("h1");
    if(capital === "I don't know") {
        element.innerHTML = "AAAAAHH!";
        element.style = "color: red;";
    } else if(capital.toLocaleLowerCase().trim() === "damaskus") {
        element.innerHTML = "That is correct";
        element.style = "color: lightgreen;";
    } else {
        element.innerHTML = "That is incorrect"; 
    }
    document.querySelector("body").append(element);
}
